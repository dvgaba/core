package com.simplerules.core.actions


import com.simplerules.core.Field
import com.simplerules.core.rules.domain.SimpleRule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class StringActionTest {

    @Test
    fun oneInOp() {
        val rule = SimpleRule("1", "Test")
        rule.actions.add(SimpleRule.Action(Field.of("name", "Chars"), "sonata", Chars.ASSIGN))
        val conditions = rule.action()
        assertThat(conditions).hasSize(1).contains("name = 'sonata'")

    }
}