package com.simplerules.core.actions

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test

internal class CharsTest {
    @Test
    fun test() {
        val mapper = ObjectMapper()

        val json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(Chars.ASSIGN)
        println(json)
        val op: ActionOperation = mapper.readValue(json, ActionOperation::class.java)
        println(op)
    }
}