package com.simplerules.core.rules

import com.fasterxml.jackson.databind.ObjectMapper
import com.simplerules.core.Field
import com.simplerules.core.ops.Chars
import com.simplerules.core.rules.domain.SimpleRule
import org.junit.jupiter.api.Test

internal class SimpleRuleTest {

    @Test
    fun base() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("fact?.name", "Chars"), "elantra,sonata,palisade", Chars.IN))
        rule.actions.add(
            SimpleRule.Action(
                Field.of("fact.brand", "Chars"),
                "hyundai",
                com.simplerules.core.actions.Chars.ASSIGN
            )
        )

        println(ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(rule))

    }
}