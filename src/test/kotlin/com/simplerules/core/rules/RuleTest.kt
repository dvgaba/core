package com.simplerules.core.rules

import com.fasterxml.jackson.databind.ObjectMapper
import com.simplerules.core.Field
import com.simplerules.core.ops.Chars
import com.simplerules.core.rules.domain.SimpleRule
import org.assertj.core.api.Assertions
import org.jeasy.rules.api.Fact
import org.jeasy.rules.api.Facts
import org.jeasy.rules.api.Rules
import org.jeasy.rules.core.DefaultRulesEngine
import org.junit.jupiter.api.Test

internal class RuleTest {

    @Test
    fun base() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("fact.name", "Chars"), "", Chars.IS_NULL))
        rule.conditions.add(SimpleRule.Condition(Field.of("fact.name", "Chars"), "hyundai", Chars.EQUALS))
        rule.actions.add(
            SimpleRule.Action(
                Field.of("fact.name", "Chars"),
                "honda",
                com.simplerules.core.actions.Chars.ASSIGN
            )
        )
        data class Car(var name: String?, var brand: String?)

        val mvelRule = rule.toRule()
        val ruleEngine = DefaultRulesEngine()
        val rules = Rules(mvelRule)
        val car = Car(null, "")
        val facts = Facts()
        val fact = Fact("fact", car)

        facts.add(fact)

        ruleEngine.fire(rules, facts)
        println(car)
    }

    @Test
    fun stringInRule() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("fact?.name", "Chars"), "elantra,sonata,palisade", Chars.IN))
        rule.actions.add(
            SimpleRule.Action(
                Field.of("fact.brand", "Chars"),
                "hyundai",
                com.simplerules.core.actions.Chars.ASSIGN
            )
        )
        data class Car(var name: String?, var brand: String?)


        val mvelRule = rule.toRule()
        val ruleEngine = DefaultRulesEngine()
        val rules = Rules(mvelRule)
        val inTest: Array<String> = arrayOf("elantra", "sonata", "palisade")
        inTest.onEach {
            val car = Car(it, "")
            val facts = Facts()
            val fact = Fact("fact", car)
            facts.add(fact)
            ruleEngine.fire(rules, facts)
            Assertions.assertThat(car.brand).isEqualTo("hyundai")
        }

        val outTest: Array<String> = arrayOf("crv", "hrv")
        outTest.onEach {
            val car = Car(it, "")
            val facts = Facts()
            val fact = Fact("fact", car)
            facts.add(fact)
            ruleEngine.fire(rules, facts)
            Assertions.assertThat(car.brand).isNotEqualTo("hyundai")
        }

    }

    @Test
    fun multipleConditions() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("source.name", "Chars"), "elantra,sonata,palisade", Chars.IN))
        rule.conditions.add(SimpleRule.Condition(Field.of("source.brand", "Chars"), "hyundai", Chars.IN))

        rule.conditions.elementAt(1).conditions.add(
            SimpleRule.Condition(
                Field.of("source.brand", "Chars"),
                "hyundai",
                Chars.EQUALS
            )
        )

        rule.actions.add(
            SimpleRule.Action(
                Field.of("source.brand", "Chars"),
                "SUCCESS",
                com.simplerules.core.actions.Chars.PRINT
            )
        )
        rule.actions.add(
            SimpleRule.Action(
                Field.of("target.message", "Chars"),
                "hey",
                com.simplerules.core.actions.Chars.ASSIGN
            )
        )
        data class Car(var name: String?, var brand: String?)


        val mvelRule = rule.toRule()
        val ruleEngine = DefaultRulesEngine()
        val rules = Rules(mvelRule)

        val car = Car("elantra", "hyundai")
        val facts = Facts()
        val fact = Fact("source", car)
        val value = HashMap<String, String>()
        val target = Fact("target", value)
        facts.add(fact)
        facts.add(target)
        ruleEngine.fire(rules, facts)
        Assertions.assertThat(value).containsKey("message").containsValue("hey")

        println(ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(rule))


    }
}


