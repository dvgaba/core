package com.simplerules.core.ops

import com.simplerules.core.Field
import com.simplerules.core.rules.domain.SimpleRule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class StringOpsTest {

    private val op = Chars.EQUALS

    @Test
    fun oneInOp() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "hyundai,sonata", Chars.IN))
        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("['hyundai','sonata'].contains(name)")
    }

    @Test
    fun one() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "hyundai", op))
        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("name?.equals('hyundai')")

    }

    @Test
    fun andTwo() {
        val rule = SimpleRule("1", "Test")
        val condition = SimpleRule.Condition(Field.of("name", "Chars"), "hyundai", op)
        condition.conditions.add(SimpleRule.Condition(Field.of("brand", "Chars"), "hyundai", op))
        rule.conditions.add(condition)
        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("(name?.equals('hyundai') && brand?.equals('hyundai'))")
    }

    @Test
    fun orTwo() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "hyundai", op))
        rule.conditions.add(SimpleRule.Condition(Field.of("brand", "Chars"), "hyundai", op))
        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("name?.equals('hyundai') || brand?.equals('hyundai')")
    }

    @Test
    fun nestedAnd() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "hyundai", op))
        val inner = SimpleRule.Condition(Field.of("brand", "Chars"), "hyundai", op)
        inner.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "elantra", op))
        rule.conditions.add(inner)

        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("name?.equals('hyundai') || (brand?.equals('hyundai') && name?.equals('elantra'))")
    }

    @Test
    fun deepNested() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "hyundai", op))
        val inner = SimpleRule.Condition(Field.of("brand", "Chars"), "hyundai", op)
        inner.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "elantra", op))
        inner.conditions.elementAt(0).conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "sonata", op))
        inner.conditions.elementAt(0).conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "palisade", op))
        rule.conditions.add(inner)

        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("name?.equals('hyundai') || (brand?.equals('hyundai') && (name?.equals('elantra') && (name?.equals('sonata') || name?.equals('palisade'))))")
    }

    @Test
    fun fieldEqualOther() {
        val rule = SimpleRule("1", "Test")
        rule.conditions.add(SimpleRule.Condition(Field.of("name", "Chars"), "\$brand", op))
        val conditions = rule.condition()
        assertThat(conditions).isEqualTo("name?.equals(brand)")

    }


}