package com.simplerules.core.onboarding

import com.simplerules.core.common.domain.UserInfo
import com.simplerules.core.onboarding.dao.OrganizationDao
import com.simplerules.core.onboarding.domain.Organization
import org.assertj.core.api.Assertions
import org.jeasy.random.EasyRandom
import org.jeasy.random.EasyRandomParameters
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import reactor.core.publisher.Mono
import java.util.*

internal class OnboardingServiceTest {

    @Test
    fun test() {
        data class Data(val id: String, val data: List<String>, val complexData: Map<String, String>)

        val data = EasyRandom(EasyRandomParameters().collectionSizeRange(3, 3)).nextObject(Data::class.java)
        println(data)
    }

    @Test
    fun appUpdate() {
        val organization = Organization(organizationId = "1", "ORG", ArrayList(), UserInfo("TEST"), Date(), Date())
        val organizationDao = Mockito.mock(OrganizationDao::class.java)
        Mockito.`when`(organizationDao.findById(Mockito.anyString())).thenReturn(Mono.just(organization))
        Mockito.`when`(organizationDao.save(Mockito.any())).thenReturn(Mono.just(organization))
        val onboardingService = OnboardingService(organizationDao)
        onboardingService.createNewApplication("1", "TEST_APP", UserInfo("TEST")).subscribe()
        Assertions.assertThat(organization.applications).hasSize(1)


    }
}