package com.simplerules.core.ops

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.springframework.data.mongodb.core.mapping.Field


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "name")
@JsonSubTypes(
    JsonSubTypes.Type(
        value = Chars.In::class, name = "CharsIn"


    ),
    JsonSubTypes.Type(
        value = Chars.Nin::class, name = "CharsNin"


    ),
    JsonSubTypes.Type(
        value = Chars.Equals::class, name = "CharsEquals"
    ),
    JsonSubTypes.Type(
        value = Chars.StartsWith::class, name = "CharsStarsWith"
    ),
    JsonSubTypes.Type(
        value = Chars.Contains::class, name = "CharsContains"
    ),
    JsonSubTypes.Type(
        value = Chars.IsNull::class, name = "CharsIsNull"
    ),
    JsonSubTypes.Type(
        value = Numeric.LessThan::class, name = "NumericLessThan"
    ),
    JsonSubTypes.Type(
        value = Numeric.LessThanEquals::class, name = "NumericLessThanEquals"
    ),
    JsonSubTypes.Type(
        value = Numeric.GreaterThan::class, name = "NumericGreaterThan"
    ),
    JsonSubTypes.Type(
        value = Numeric.GreaterThanEquals::class, name = "NumericGreaterThanEquals"
    ),
    JsonSubTypes.Type(
        value = Numeric.Equals::class, name = "NumericEquals"
    ),
    JsonSubTypes.Type(
        value = Numeric.StartsWith::class, name = "NumericStartsWith"
    ),
    JsonSubTypes.Type(
        value = Numeric.Contains::class, name = "NumericContains"
    ),
    JsonSubTypes.Type(
        value = Numeric.In::class, name = "NumericIn"
    ),
    JsonSubTypes.Type(
        value = Numeric.Nin::class, name = "NumericNin"
    )
)
abstract class Operation internal constructor(var operationId: String, var displayName: String, var path: String) {
    @Field
    open var multiValue: Boolean = false


    abstract fun value(input: String): String
}