package com.simplerules.core.ops

import java.util.stream.Collectors

open class Chars {

    class IsNull : Operation("IS_NULL", "is null", "\${key} == null") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class Equals : Operation("EQUALS", "Equals", "\${key}?.equals(\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class StartsWith : Operation("STARTS_WITH", "Starts With", "\${key}.startsWith(\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class Contains : Operation("CONTAINS", "Contains", "\${key}.contains(\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class In : Operation("IN", "In", "[\${value}].contains(\${key})") {
        init {
            super.multiValue = true
        }

        override fun value(input: String): String {

            return input.split(",").stream().map { "'$it'" }.collect(Collectors.joining(","))
        }
    }

    class Nin : Operation("NIN", "Not in", "[\${value}].contains(\${key}) == false") {
        override fun value(input: String): String {
            return input.split(",").stream().map { "'$it'" }.collect(Collectors.joining(","))
        }
    }


    companion object OPS {
        val IS_NULL: Operation = IsNull()
        val EQUALS: Operation = Equals()
        val STARTS_WITH: Operation = StartsWith()
        val CONTAINS: Operation = Contains()
        val IN: Operation = In()
        val NIN: Operation = Nin()
        val availableOperations = OperationList(arrayListOf<Operation>(EQUALS, IN, STARTS_WITH, CONTAINS, NIN))

    }
}