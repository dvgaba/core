package com.simplerules.core.ops

import java.util.stream.Collectors

class Numeric {

    class LessThan : Operation("LESS_THAN", " < ", "\${key} < (\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class LessThanEquals : Operation("LESS_THAN_EQUALS", " <= ", "\${key} < \${value}") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class GreaterThanEquals : Operation("GREATER_THAN_EQUALS", " >= ", "\${key} < \${value}") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class GreaterThan : Operation("GREATER_THAN_EQUALS", " > ", "\${key} < \${value}") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class Equals : Operation("EQUALS", " == ", "\${key} == \${value}") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class StartsWith : Operation("STARTS_WITH", "Starts With", "\${key}.startsWith(\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class Contains : Operation("CONTAINS", "Contains", "\${key}.contains(\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class In : Operation("IN", "In", "[\${value}].contains(\${key})") {
        override fun value(input: String): String {
            return input.split(",").stream().map { "'$it'" }.collect(Collectors.joining(","))
        }
    }

    class Nin : Operation("NIN", "Not in", "[\${value}].contains(\${key}) == false") {
        override fun value(input: String): String {
            return input.split(",").stream().map { "'$it'" }.collect(Collectors.joining(","))
        }
    }

    companion object OPS {

        val LESS_THAN: Operation = LessThan()

        val LESS_THAN_EQUALS: Operation = LessThanEquals()

        val GREATER_THAN_EQUALS: Operation = GreaterThanEquals()

        val GREATER_THAN: Operation = GreaterThan()

        val EQUALS: Operation = Equals()

        val STARTS_WITH: Operation = StartsWith()

        val CONTAINS: Operation = Contains()

        val IN: Operation = In()

        val NIN: Operation = Nin()

        val availableOperations = OperationList(
            arrayListOf<Operation>(
                LESS_THAN,
                LESS_THAN_EQUALS, GREATER_THAN_EQUALS, GREATER_THAN, EQUALS, IN, STARTS_WITH, CONTAINS, NIN
            )
        )
    }
}