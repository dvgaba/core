package com.simplerules.core

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@SpringBootApplication
@EnableReactiveMongoRepositories
class CoreApplication

fun main(args: Array<String>) {
    runApplication<CoreApplication>(*args)
}


