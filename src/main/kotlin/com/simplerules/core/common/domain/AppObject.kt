package com.simplerules.core.common.domain

import java.util.*

interface AppObject {
    val owner: UserInfo
    val createdOn: Date
    val modifiedOn: Date
}
