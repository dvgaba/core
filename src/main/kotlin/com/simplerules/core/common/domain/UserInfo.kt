package com.simplerules.core.common.domain

data class UserInfo(val userId: String)