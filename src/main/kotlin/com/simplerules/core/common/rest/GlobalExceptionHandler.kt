package com.simplerules.core.common.rest

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.io.IOException


@Configuration
@Order(-2)
class GlobalExceptionHandler : ErrorWebExceptionHandler {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)
        val objectMapper = ObjectMapper()
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handle(e: HttpMessageNotReadableException?) {
        logger.warn("Returning HTTP 400 Bad Request", e)
    }


    override fun handle(serverWebExchange: ServerWebExchange, throwable: Throwable): Mono<Void> {
        //logger.error("Voila", throwable)
        logger.error("serverWebExchange {}", serverWebExchange.response.statusCode)
        val bufferFactory = serverWebExchange.response.bufferFactory()
        val message: String = if (throwable.message == null) StringUtils.EMPTY else throwable.message!!
        if (throwable is IOException) {
            serverWebExchange.response.statusCode = HttpStatus.BAD_REQUEST
            var dataBuffer: DataBuffer?
            try {

                dataBuffer = bufferFactory.wrap(
                    objectMapper.writeValueAsBytes(
                        HttpError(
                            message
                        )
                    )
                )
            } catch (e: JsonProcessingException) {
                dataBuffer = bufferFactory.wrap("".toByteArray())
            }
            serverWebExchange.response.headers.contentType = MediaType.APPLICATION_JSON
            return serverWebExchange.response.writeWith(Mono.just(dataBuffer!!))
        }
        //serverWebExchange.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
        serverWebExchange.response.headers.contentType = MediaType.TEXT_PLAIN
        val dataBuffer: DataBuffer = bufferFactory.wrap(message.toByteArray())
        return serverWebExchange.response.writeWith(Mono.just(dataBuffer))
    }

    class HttpError internal constructor(val message: String)


}