package com.simplerules.core.rules.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.simplerules.core.RuleConfig
import com.simplerules.core.ops.Chars
import com.simplerules.core.ops.Numeric
import com.simplerules.core.rules.dao.RuleConfigRepo
import com.simplerules.core.rules.domain.SimpleRule
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.*

@Service
class RuleService(val ruleConfigRepo: RuleConfigRepo) {
    fun getConfig(ruleConfigId: String): Mono<RuleConfig> {
        return this.ruleConfigRepo.findById(ruleConfigId)
    }

    fun getMetadata(): Map<String, Any> {
        val conditionOperations = HashMap<String, Any>()
        conditionOperations["Chars"] = Chars.availableOperations
        conditionOperations["Numeric"] = Numeric.availableOperations

        val actionOperations = HashMap<String, Any>()
        actionOperations["Chars"] = com.simplerules.core.actions.Chars.availableOperations
        actionOperations["Numeric"] = com.simplerules.core.actions.Numeric.availableOperations

        val data = HashMap<String, Any>()
        data["conditionOperations"] = conditionOperations
        data["actionOperations"] = actionOperations

        return data
    }

    fun saveRules(rulesInfo: RuleConfig): Mono<RuleConfig> {
        return this.ruleConfigRepo.save(rulesInfo)
    }

    fun saveRules(rulesConfigId: String, rules: List<SimpleRule>): Mono<RuleConfig> {
        println(ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(rules))
        return this.ruleConfigRepo.findById(rulesConfigId).map { oit ->
            rules.stream().filter { it.ruleId.startsWith("NEW_") }.forEach {
                it.ruleId = UUID.randomUUID().toString()
            }
            oit.rules.clear()
            oit.rules.addAll(rules)
            oit
        }.log().flatMap {
            this.ruleConfigRepo.save(it)
        }.log()
    }
}
