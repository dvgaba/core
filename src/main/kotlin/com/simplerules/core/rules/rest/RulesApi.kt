package com.simplerules.core.rules.rest

import com.simplerules.core.RuleConfig
import com.simplerules.core.rules.domain.SimpleRule
import com.simplerules.core.rules.service.RuleService
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/simple/api/rules")
class RulesApi(val service: RuleService) {
    @GetMapping("/{ruleConfigId}")
    fun get(@PathVariable ruleConfigId: String): Mono<RuleConfig> {
        return this.service.getConfig(ruleConfigId)
    }

    @GetMapping("/ruleMetadata")
    fun metadata(): Map<String, Any> {
        return this.service.getMetadata()
    }

    @PostMapping("/{ruleConfigId}")
    fun saveRules(@PathVariable ruleConfigId: String, @RequestBody rules: List<SimpleRule>): Mono<RuleConfig> {
        return this.service.saveRules(ruleConfigId, rules)
    }

}


