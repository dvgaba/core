package com.simplerules.core.rules.domain

import com.fasterxml.jackson.annotation.JsonProperty
import com.simplerules.core.Field
import com.simplerules.core.actions.ActionOperation
import com.simplerules.core.ops.Operation
import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.StringSubstitutor
import org.jeasy.rules.api.Rule
import org.jeasy.rules.jexl.JexlRule
import java.util.stream.Collectors

data class SimpleRule(
    var ruleId: String, val ruleDescription: String, var conditions: LinkedHashSet<Condition> = LinkedHashSet(),
    var actions: LinkedHashSet<Action> = LinkedHashSet(), var ruleName: String? = ""
) {
    var priority: Int = 0


    @JsonProperty
    fun condition(): String {
        return this.conditions.stream().map { it.toString() }.collect(Collectors.joining(" || "))
    }

    @JsonProperty
    fun action(): List<String> {
        return this.actions.stream().map { it.toString() }.collect(Collectors.toList())
    }

    fun toRule(): Rule {
        val rule = JexlRule()
        rule.name(ruleId)
        rule.description(ruleDescription)
        println(condition())

        rule.`when`(condition())
        rule.then(action().toString())
        rule.priority(this.priority)
        return rule
    }

    data class Condition(var key: Field, var value: String, var operation: Operation) {
        var conditions: LinkedHashSet<Condition> = LinkedHashSet()
        override fun toString(): String {
            val data = ArrayList<String>()
            val string = getString()
            this.conditions.forEach {
                val resolvedString = it.toString()
                data.add(resolvedString)
            }
            if (data.isNotEmpty()) {
                val inner = StringUtils.join(data, " || ")
                val innerString = if (data.size == 1) inner else "($inner)"
                return "($string && $innerString)"
            }

            return string
        }

        private fun getString(): String {
            val it = this
            val valuesMap: MutableMap<String, String> = LinkedHashMap()
            valuesMap["value"] = it.operation.value(it.value)
            valuesMap["key"] = it.key.fieldPath
            val templateString = it.operation.path
            val sub = StringSubstitutor(valuesMap)
            return sub.replace(templateString)
        }
    }

    data class Action(var key: Field, var value: String, var actionOperation: ActionOperation) {
        override fun toString(): String {
            val it = this
            val valuesMap: MutableMap<String, String> = LinkedHashMap()
            valuesMap["value"] = it.actionOperation.value(it.value)
            valuesMap["key"] = it.key.fieldPath
            val templateString = it.actionOperation.path
            val sub = StringSubstitutor(valuesMap)
            return sub.replace(templateString)
        }
    }
}