package com.simplerules.core.rules.dao

import com.simplerules.core.RuleConfig
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
interface RuleConfigRepo : ReactiveMongoRepository<RuleConfig, String> {
    fun findOneByRuleConfigLabel(ruleConfigLabel: String): Mono<RuleConfig>

    fun findOneByRuleConfigLabelAndOrganizationIdAndApplicationId(
        ruleConfigLabel: String,
        applicationId: String,
        organizationId: String
    ): Mono<RuleConfig>
}
