package com.simplerules.core.actions

class Numeric {

    class Assign : ActionOperation("Assign", " = ", "\${key} = \${value}") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    companion object OPS {
        val ASSIGN: ActionOperation = Assign()
        val availableOperations = ActionOperationList(arrayListOf<ActionOperation>(ASSIGN))
    }
}