package com.simplerules.core.actions

class Chars {

    class Print : ActionOperation("Print", " = ", "System.out.println(\${value})") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    class Assign : ActionOperation("Assign", "Assign", "\${key} = \${value}") {
        override fun value(input: String): String {
            return if (input.startsWith("$")) input.replace("$", "") else "'$input'"
        }
    }

    companion object OPS {

        val PRINT: ActionOperation = Print()

        val ASSIGN: ActionOperation = Assign()

        val availableOperations = ActionOperationList(arrayListOf<ActionOperation>(ASSIGN))
    }
}