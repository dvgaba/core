package com.simplerules.core.actions

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "name")
@JsonSubTypes(
    JsonSubTypes.Type(
        value = Chars.Print::class, name = "CharsPrint"


    ),
    JsonSubTypes.Type(
        value = Chars.Assign::class, name = "CharsAssign"


    ),
    JsonSubTypes.Type(
        value = Numeric.Assign::class, name = "NumericAssign"


    )
)
abstract class ActionOperation internal constructor(
    var operationId: String,
    var displayName: String,
    var path: String
) {

    abstract fun value(input: String): String
}

