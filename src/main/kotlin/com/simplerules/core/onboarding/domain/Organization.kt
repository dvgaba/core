package com.simplerules.core.onboarding.domain

import com.simplerules.core.common.domain.AppObject
import com.simplerules.core.common.domain.UserInfo
import org.springframework.data.annotation.Id
import java.util.*

data class Organization(
    @Id var organizationId: String?,
    val organizationLabel: String,
    val applications: MutableList<Application>,
    override val owner: UserInfo,
    override val createdOn: Date,
    override val modifiedOn: Date,

    ) : AppObject