package com.simplerules.core.onboarding

import com.simplerules.core.Field
import com.simplerules.core.common.domain.UserInfo
import com.simplerules.core.onboarding.dao.OrganizationDao
import com.simplerules.core.onboarding.domain.Application
import com.simplerules.core.onboarding.domain.Organization
import com.simplerules.core.ops.Chars
import com.simplerules.core.ops.Numeric
import org.apache.commons.text.WordUtils
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.*

@Service
class OnboardingService(internal val organizationDao: OrganizationDao) {

    fun createNewOrganization(organizationLabel: String, userInfo: UserInfo): Mono<Organization> {
        val organization = Organization(null, organizationLabel, ArrayList(), userInfo, Date(), Date())
        return organizationDao.insert(organization)
    }

    fun createNewApplication(
        organizationId: String,
        applicationLabel: String,
        userInfo: UserInfo
    ): Mono<Organization?> {
        return this.organizationDao.findById(organizationId)
            .map {
                it.applications.add(Application(null, applicationLabel, UserInfo("APP"), Date(), Date()))
                it
            }.flatMap {
                this.organizationDao.save(it)
            }
    }

    fun getKeysForMap(data: Map<String, Any>) = toInternalTypes(JsonMapFlattener.flatten(data) as Map<String, String>)

    fun getKeysForMap(data: List<Map<String, Any>>) = toInternalTypes(JsonMapFlattener.flattenToStringMap(data))

    private fun toInternalTypes(data: Map<String, String>): List<Field> {
        val output = ArrayList<Field>()
        data.entries.stream().forEach {
            when (it.value) {
                ""::class.java.name -> {
                    val field = Field(toDisplayName(it.key), it.key, Chars::class.simpleName.toString(), null)
                    output.add(field)
                }
                1::class.java.name -> {
                    val field = Field(toDisplayName(it.key), it.key, Numeric::class.simpleName.toString(), null)
                    output.add(field)
                }
                else -> {
                    throw IllegalStateException("Unknown Type " + it.value)
                }
            }

        }
        return output
    }

    private fun toDisplayName(str: String): String {
        return WordUtils.capitalizeFully(str.replace(".", " "))
    }

}