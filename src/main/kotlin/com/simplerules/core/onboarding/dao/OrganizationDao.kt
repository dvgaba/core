package com.simplerules.core.onboarding.dao

import com.simplerules.core.onboarding.domain.Organization
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface OrganizationDao : ReactiveMongoRepository<Organization, String>