package com.simplerules.core.onboarding;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.UnaryOperator;

/**
 * Flattens a hierarchical {@link Map} of objects into a property {@link Map}.
 * <p>
 * Flattening is particularly useful when representing a JSON object as
 * {@link java.util.Properties}
 * <p>
 * {@link JsonMapFlattener} flattens {@link Map maps} containing nested
 * {@link java.util.List}, {@link Map} and simple values into a flat representation. The
 * hierarchical structure is reflected in properties using dot-notation. Nested maps are
 * considered as sub-documents.
 * <p>
 * Input:
 *
 * <pre class="code">
 *     {"key": {"nested: 1}, "another.key": ["one", "two"] }
 * </pre>
 *
 * <br>
 * Result
 *
 * <pre class="code">
 *  key.nested=1
 *  another.key[0]=one
 *  another.key[1]=two
 * </pre>
 *
 * @author Mark Paluch
 */
public interface JsonMapFlattener {

    String LIST = "List.";
    String NESTED_LIST = "NestedList.";

    /**
     * Flatten a hierarchical {@link Map} into a flat {@link Map} with key names using
     * property dot notation.
     *
     * @param inputMap must not be {@literal null}.
     * @return the resulting {@link Map}.
     */
    static Map<String, Object> flatten(Map<String, ? extends Object> inputMap) {

        Assert.notNull(inputMap, "Input Map must not be null");

        Map<String, Object> resultMap = new LinkedHashMap<>();

        doFlatten("", inputMap.entrySet().iterator(), resultMap, UnaryOperator.identity());

        return resultMap;
    }

    /**
     * Flatten a hierarchical {@link Map} into a flat {@link Map} with key names using
     * property dot notation.
     *
     * @param inputMap must not be {@literal null}.
     * @return the resulting {@link Map}.
     * @since 2.0
     */
    static Map<String, String> flattenToStringMap(Object inputMap) {

        Assert.notNull(inputMap, "Input Map must not be null");

        Map<String, String> resultMap = new LinkedHashMap<>();

        flattenElement("", inputMap, resultMap, it -> it == null ? null : it.toString());

        return resultMap;
    }

    private static void doFlatten(String propertyPrefix, Iterator<? extends Entry<String, ?>> inputMap,
                                  Map<String, ? extends Object> resultMap, UnaryOperator<Object> valueTransformer) {

        if (StringUtils.hasText(propertyPrefix) && !propertyPrefix.equals(LIST)) {
            propertyPrefix = propertyPrefix + ".";
        }

        while (inputMap.hasNext()) {

            Entry<String, ? extends Object> entry = inputMap.next();
            flattenElement(propertyPrefix.concat(entry.getKey()), entry.getValue(), resultMap, valueTransformer);
        }
    }

    @SuppressWarnings("unchecked")
    private static void flattenElement(String propertyPrefix, @Nullable Object source, Map<String, ?> resultMap,
                                       UnaryOperator<Object> valueTransformer) {

        if (source == null)
            return;

        if (source instanceof Iterable) {
            flattenCollection(propertyPrefix, (Iterable<Object>) source, resultMap, valueTransformer);
            return;
        }

        if (source instanceof Map) {
            doFlatten(propertyPrefix, ((Map<String, ?>) source).entrySet().iterator(), resultMap, valueTransformer);
            return;
        }

        ((Map) resultMap).put(propertyPrefix, source.getClass().getCanonicalName());
    }

    private static void flattenCollection(String propertyPrefix, Iterable<Object> iterable, Map<String, ?> resultMap,
                                          UnaryOperator<Object> valueTransformer) {
        if (iterable != null && iterable.iterator().hasNext()) {
            Object next = iterable.iterator().next();
            if (next instanceof Map) {
                doFlatten(getListPrefixed(propertyPrefix), ((Map<String, ?>) iterable.iterator().next()).entrySet().iterator(), resultMap, valueTransformer);
                return;
            }

            ((Map) resultMap).put(propertyPrefix, iterable.getClass().getCanonicalName() + "_" + next.getClass().getCanonicalName());
        }
    }

    @NotNull
    private static String getListPrefixed(String propertyPrefix) {
        if (!propertyPrefix.startsWith(LIST)) {
            return LIST + propertyPrefix;
        } else {
            return NESTED_LIST + propertyPrefix;
        }


    }

}