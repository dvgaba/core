package com.simplerules.core.onboarding.rest

import com.simplerules.core.Field
import com.simplerules.core.onboarding.OnboardingService
import com.simplerules.core.ruleconfig.RuleConfigService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/simple/api/onboarding")
class OnboardingApi(val onboardingService: OnboardingService, val ruleConfigService: RuleConfigService) {
    @PostMapping("get/keys", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun get(@RequestBody data: Any): List<Field> {
        if (data is Map<*, *>) {
            return this.onboardingService.getKeysForMap(data as Map<String, Any>)
        }
        if (data is List<*>) {
            return this.onboardingService.getKeysForMap(data as List<Map<String, Any>>)
        } else {
            throw IllegalArgumentException("Invalid input")
        }
    }


}