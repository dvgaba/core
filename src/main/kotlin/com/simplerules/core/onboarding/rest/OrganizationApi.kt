package com.simplerules.core.onboarding.rest

import com.simplerules.core.common.domain.UserInfo
import com.simplerules.core.onboarding.OnboardingService
import com.simplerules.core.onboarding.domain.Organization
import com.simplerules.core.ruleconfig.RuleConfigService
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/simple/api/organization")
class OrganizationApi(val onboardingService: OnboardingService, val ruleConfigService: RuleConfigService) {
    @GetMapping("/get/all")
    fun getOrgs(): Flux<Organization> {
        return this.onboardingService.organizationDao.findAll()
    }

    @PostMapping()
    fun createNewOrganization(@RequestParam organizationLabel: String): Mono<Organization> {
        return this.onboardingService.createNewOrganization(organizationLabel, UserInfo("APP"))
    }

    @PostMapping("/add/application")
    fun createNewApplication(
        @RequestParam organizationId: String,
        @RequestParam applicationLabel: String
    ): Mono<Organization?> {
        return this.onboardingService.createNewApplication(organizationId, applicationLabel, UserInfo("APP"))
    }
}