package com.simplerules.core.ruleconfig

import com.simplerules.core.Field
import com.simplerules.core.RuleConfig
import com.simplerules.core.rules.dao.RuleConfigRepo
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service

class RuleConfigService(val ruleConfigRepo: RuleConfigRepo) {
    fun create(
        ruleConfigLabel: String,
        fields: List<Field>,
        organizationId: String,
        applicationId: String
    ): Mono<RuleConfig> {
        val ruleConfig = RuleConfig(null, ruleConfigLabel, organizationId, applicationId, fields)
        return this.ruleConfigRepo.insert(ruleConfig)
    }

    fun get(ruleConfigLabel: String, organizationId: String, applicationId: String): Mono<RuleConfig> {
        return this.ruleConfigRepo.findOneByRuleConfigLabelAndOrganizationIdAndApplicationId(
            ruleConfigLabel,
            applicationId,
            organizationId
        )
    }
}
