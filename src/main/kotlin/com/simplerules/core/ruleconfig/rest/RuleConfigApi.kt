package com.simplerules.core.ruleconfig.rest

import com.simplerules.core.Field
import com.simplerules.core.RuleConfig
import com.simplerules.core.ruleconfig.RuleConfigService
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono


@RestController
@RequestMapping("/simple/api/ruleConfig")
class RuleConfigApi(val ruleConfigService: RuleConfigService) {
    @PostMapping
    fun create(
        @RequestParam ruleConfigLabel: String,
        @RequestBody fields: List<Field>,
        @RequestHeader organizationId: String,
        @RequestHeader applicationId: String
    ): Mono<RuleConfig> {
        return this.ruleConfigService.create(ruleConfigLabel, fields, organizationId, applicationId)
    }

    @GetMapping()
    fun get(
        @RequestParam ruleConfigLabel: String,
        @RequestHeader organizationId: String,
        @RequestHeader applicationId: String
    ): Mono<RuleConfig> {
        return this.ruleConfigService.get(ruleConfigLabel, organizationId, applicationId)
    }
}