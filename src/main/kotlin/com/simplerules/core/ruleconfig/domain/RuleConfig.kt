package com.simplerules.core

import com.simplerules.core.rules.domain.SimpleRule
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "ruleConfigs")
data class RuleConfig(
    @Id val ruleConfigId: String?,
    val ruleConfigLabel: String,
    val organizationId: String,
    val applicationId: String,
    val fields: List<Field>,

    ) {
    var rules: MutableList<SimpleRule> = ArrayList()
}

data class Field(
    val fieldDisplayName: String,
    val fieldPath: String,
    val fieldType: String,
    val values: List<String>?
) {
    companion object {
        fun of(key: String, fieldType: String): Field {
            return Field(key, key, fieldType, null)
        }
    }
}